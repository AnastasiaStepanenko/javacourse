package ServiceTwo;

import ShipsPort.Schedule;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Converter {
    Gson gson;
    String JSON;

    public Converter(Schedule schedule) {
        gson = new Gson();
        JSON  = gson.toJson(schedule);
        System.out.println (JSON);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream("report.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            out.write(JSON.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Gson getGson(){
        return gson;
    }

    public String getJSON(){
        return JSON;
    }
}
