package ServiceThree;

import ShipsPort.Schedule;
import ShipsPort.Ship;

import java.util.PriorityQueue;
import java.util.Random;

public class Model {
    Schedule schedule;
    int fine;
    int maxDay = 7;
    int minDay = -7;
    int maxAnchorage = 1440;

    public Model(Schedule schedule){
        this.schedule = schedule;
    }

    //TODO: Create fine checker
    public boolean isFine(){
        return true;
    }

    public void simulate() {
        PriorityQueue<Ship> looseCp = new PriorityQueue<>();
        PriorityQueue<Ship> liquidCp = new PriorityQueue<>();
        PriorityQueue<Ship> containerCp = new PriorityQueue<>();
        Random rand = new Random();
        int day = 0, anchorage = 0;
        Ship ship;
        while (!schedule.getQueueLoose().isEmpty()) {
            ship = schedule.getQueueLoose().poll();
            day = ship.getDay();
            ship.setDay(day + rand.nextInt(maxDay - minDay + 1) - 7);
            anchorage = ship.getAnchorage();
            ship.setAnchorage(anchorage + rand.nextInt(maxAnchorage));
            looseCp.add(ship);
            //TODO: Add fines for each hour
        }
        while (!schedule.getQueueLiquid().isEmpty()) {
            ship = schedule.getQueueLiquid().poll();
            day = ship.getDay();
            ship.setDay(day + rand.nextInt(maxDay - minDay + 1) - 7);
            anchorage = ship.getAnchorage();
            ship.setAnchorage(anchorage + rand.nextInt(maxAnchorage));
            liquidCp.add(ship);
        }
        while (!schedule.getQueueContainer().isEmpty()) {
            ship = schedule.getQueueContainer().poll();
            day = ship.getDay();
            ship.setDay(day + rand.nextInt(maxDay - minDay + 1) - 7);
            anchorage = ship.getAnchorage();
            ship.setAnchorage(anchorage + rand.nextInt(maxAnchorage));
            containerCp.add(ship);
        }
        schedule.setQueueLoose(looseCp);
        schedule.setQueueLiquid(liquidCp);
        schedule.setQueueContainer(containerCp);
    }
}
