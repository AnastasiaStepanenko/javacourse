package ShipsPort;

import java.util.PriorityQueue;
import java.util.Scanner;

public class Schedule {
    PriorityQueue<Ship> queueLoose;
    PriorityQueue<Ship> queueLiquid;
    PriorityQueue<Ship> queueContainer;

    public Schedule(){
        queueLoose = new PriorityQueue<>();
        queueLiquid = new PriorityQueue<>();
        queueContainer = new PriorityQueue<>();
    }

    public PriorityQueue<Ship> getQueueLoose(){
        return queueLoose;
    }

    public PriorityQueue<Ship> getQueueLiquid(){
        return queueLiquid;
    }

    public PriorityQueue<Ship> getQueueContainer(){
        return queueContainer;
    }

    public void setQueueLoose(PriorityQueue<Ship> queueLoose){
        this.queueLoose = queueLoose;
    }

    public void setQueueLiquid(PriorityQueue<Ship> queueLiquid){
        this.queueLiquid = queueLiquid;
    }

    public void setQueueContainer(PriorityQueue<Ship> queueContainer){
        this.queueContainer = queueContainer;
    }

    public void printInfo() {
        PriorityQueue<Ship> looseCp = new PriorityQueue<>(queueLoose);
        PriorityQueue<Ship> liquidCp = new PriorityQueue<>(queueLiquid);
        PriorityQueue<Ship> containerCp = new PriorityQueue<>(queueContainer);
        System.out.println("Loose type of ships: \n");
        Ship ship;
        while (!looseCp.isEmpty()) {
            ship = looseCp.poll();
            ship.print();
        }
        System.out.println("Liquid type of ships: \n");
        while (!liquidCp.isEmpty()) {
            ship = liquidCp.poll();
            ship.print();
        }
        System.out.println("Container type of ships: \n");
        while (!containerCp.isEmpty()) {
            ship = containerCp.poll();
            ship.print();
        }
    }

    //TODO: console input
    public void addNote(Ship ship){
      //  System.out.println("Enter name of ship: ", );
        Cargo type = ship.getType();
        if (type.equals(Cargo.LOOSE)){
            queueLoose.add(ship);
        } else if (type.equals(Cargo.LIQUID)){
            queueLiquid.add(ship);
        } else if (type.equals(Cargo.CONTAINER)){
            queueContainer.add(ship);
        }
    }

    @Override
    public String toString() {
        return "QueueLoose{" + queueLoose + '}' +
                "QueueLiquid{" + queueLiquid + '}' +
                "QueueContainer{" + queueContainer + '}';
    }
}


