package ServiceOne;

import ShipsPort.Cargo;
import ShipsPort.Crane;
import ShipsPort.Schedule;
import ShipsPort.Ship;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Generator {
    Schedule board;

    public Generator(Schedule board){
        this.board = board;
    }

    private void readFromFile(List<String> names) throws IOException {
        Scanner input = new Scanner(System.in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("names.txt")));
        String name = "";
        while (name != null) {
            name = reader.readLine();
            if (name != null) {
                names.add(name);
            }
        }
    }

    private void generateName(Ship ship) throws IOException {
        Random rand = new Random();
        List<String> names = new ArrayList<>();
        readFromFile(names);
        ship.setName(names.get(rand.nextInt(25)));
    }

    private void generateDate(Ship ship){
        Random rand = new Random();
        int day = rand.nextInt(31);
        int hour = rand.nextInt(25);
        int minute = rand.nextInt(61);
        ship.setDate(day, hour, minute);
    }

    private void generateWeight(Ship ship, Cargo type) {
        Random rand = new Random();
        if (type.equals(Cargo.LOOSE) || type.equals(Cargo.LIQUID)) {
            ship.setWeight(rand.nextInt(200000));
        }
        else if (type.equals(Cargo.CONTAINER)) {
            ship.setWeight(rand.nextInt(4500));
        }
    }

    public void generateSchedule() throws IOException {
        Random rand = new Random();
        Cargo type;
        Ship ship;
        Crane crane;
        for (int i = 0; i <= 1000; ++i) {
        int param = rand.nextInt(3);
        switch (param) {
            case (0):
                type = Cargo.LOOSE;
                ship = new Ship(type);
                crane = new Crane(type);
                generateDate(ship);
                generateWeight(ship, type);
                generateName(ship);
                ship.setAnchorage(ship.getWeight() / crane.getProductivity());
                board.getQueueLoose().add(ship);
                break;
            case (1):
                type = Cargo.LIQUID;
                ship = new Ship(type);
                crane = new Crane(type);
                generateDate(ship);
                generateWeight(ship, type);
                generateName(ship);
                ship.setAnchorage(ship.getWeight() / crane.getProductivity());
                board.getQueueLiquid().add(ship);
                break;
            case (2):
                type = Cargo.CONTAINER;
                ship = new Ship(type);
                crane = new Crane(type);
                generateDate(ship);
                generateWeight(ship, type);
                generateName(ship);
                ship.setAnchorage(ship.getWeight() / crane.getProductivity());
                board.getQueueContainer().add(ship);
                break;
            }
        }
    }
}
