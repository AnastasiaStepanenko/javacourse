package ShipsPort;

public enum Cargo {
    LOOSE,
    LIQUID,
    CONTAINER
}
