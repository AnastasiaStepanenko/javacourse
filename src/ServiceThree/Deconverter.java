package ServiceThree;

import ShipsPort.Schedule;
import com.google.gson.Gson;

public class Deconverter {
    Schedule schedule;

    public Deconverter(Gson gson, String JSON) {
        schedule = gson.fromJson(JSON, Schedule.class);
    }

    public Schedule getSchedule(){
        return schedule;
    }
}
