package ShipsPort;

import java.util.Scanner;

public class Ship implements Comparable{
    String name;
    Cargo type;
    int weight;
    int anchorage; //TODO: Create logical sense for this param (is this minutes or hours?)
    int day;
    int hour;
    int minute;

    public Ship(Cargo type) {
        this.type = type;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setWeight(int weight){
        this.weight = weight;
    }

    public void setAnchorage(int anchorage){
        this.anchorage = anchorage;
    }

    public void setDate(int day, int hour, int minute){
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }

    public void setDay(int day){
        this.day = day;
    }

    public Cargo getType(){
        return type;
    }

    public int getWeight(){
        return weight;
    }

    public int getDay(){
        return day;
    }

    public int getAnchorage(){
        return anchorage;
    }

    public void consoleEnter(){
        Scanner in = new Scanner(System.in);

    }

    public void print(){
        System.out.println("Name: " + name + "\nData: " + day + "." + hour + "." + minute);
        System.out.println("Weight: " + weight + "\nAnchorage: " + anchorage + '\n');
    }

    @Override
    public int compareTo(Object o) {
        Ship ship = ((Ship) o);
        if (ship.day == day) {
            if (ship.hour == hour){
                if (ship.minute == minute){
                    return 0;
                } else {
                    return ship.minute < minute ? 1 : -1;
                }
            } else {
                return ship.hour < hour ? 1 : -1;
            }
        } else {
            return ship.day < day ? 1 : -1;
        }
    }

    @Override
    public String toString() {
        return "Ship {" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", weight=" + weight +
                ", anchorage=" + anchorage +
                ", day=" + day +
                ", hour=" + hour +
                ", minute=" + minute +
                '}';
    }
}
