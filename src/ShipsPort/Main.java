package ShipsPort;

import ServiceOne.Generator;
import ServiceThree.Deconverter;
import ServiceTwo.Converter;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Schedule board = new Schedule();
        //Thread thr = new Thread(() -> {
        //
        //});
        Generator generator = new Generator(board);
        generator.generateSchedule();
        board.printInfo();
        Converter converter = new Converter(board);
        Deconverter deconverter = new Deconverter(converter.getGson(), converter.getJSON());

        Crane crane = new Crane(Cargo.LOOSE);
        Crane crane1 = new Crane(Cargo.LIQUID);
        Thread thread = new Thread(crane);
        Thread thread1 = new Thread(crane1);
        thread.start();
        thread1.start();
    }
}
